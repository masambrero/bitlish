import * as actionTypes from 'Constants/ActionTypes';

export const showModal = (info, error) => ({
  type: actionTypes.SHOW_MODAL,
  info,
  error,
});

export const hideModal = () => ({
  type: actionTypes.HIDE_MODAL,
});

export const receiveOrderBook = orderBook => ({
  type: actionTypes.RECEIVE_ORDER_BOOK,
  data: orderBook,
});

const showOrderPrice = price => ({
  type: actionTypes.SHOW_ORDER_PRICE,
  price,
});

export const calculatePrice = ({ orderType, orderValue }) => (dispatch, getState) => {
  const state = getState();
  if (state.order.total[orderType] < orderValue) return dispatch(showModal('Превышаете?!'));
  const sortedOrderBook = state.order.orderBook[orderType].sort((a, b) => (a.price - b.price));
  let totalPrice = 0;
  let volumeLeftToBuy = orderValue;
  sortedOrderBook.forEach((current) => {
    if (volumeLeftToBuy <= 0) return;
    if (volumeLeftToBuy > current.volume) {
      totalPrice += current.price * current.volume;
      volumeLeftToBuy -= current.volume;
    } else {
      totalPrice += current.price * volumeLeftToBuy;
      volumeLeftToBuy = 0;
    }
  });
  return dispatch(showOrderPrice(totalPrice.toFixed(2)));
};
