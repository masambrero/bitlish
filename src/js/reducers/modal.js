import {
  SHOW_MODAL, HIDE_MODAL,
} from 'Constants/ActionTypes';

const initialState = {
  modalShown: false,
  modalInfo: null,
  error: false,
};

const modal = (state = initialState, action) => {
  switch (action.type) {
    case SHOW_MODAL:
      return {
        ...state,
        modalShown: true,
        modalInfo: action.info,
        error: action.error,
      };
    case HIDE_MODAL:
      return initialState;
    default:
      return state;
  }
};

export default modal;
