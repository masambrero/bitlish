import {
  RECEIVE_ORDER_BOOK, SHOW_ORDER_PRICE,
} from 'Constants/ActionTypes';

const initialState = {
  orderBook: null,
  orderPrice: 0,
  outdated: true,
};

const order = (state = initialState, action) => {
  let maxAskVolume;
  let maxBidVolume;
  let areBooksEqual;
  switch (action.type) {
    case RECEIVE_ORDER_BOOK:
      areBooksEqual = JSON.stringify(action.data) === JSON.stringify(state.orderBook);
      maxAskVolume = action.data.ask.reduce((prev, current) => (prev + parseInt(current.volume, 10)), 0);
      maxBidVolume = action.data.bid.reduce((prev, current) => (prev + parseInt(current.volume, 10)), 0);
      return {
        ...state,
        outdated: state.orderPrice !== 0 && !areBooksEqual,
        total: { ask: maxAskVolume, bid: maxBidVolume },
        orderBook: action.data,
      };
    case SHOW_ORDER_PRICE:
      return {
        ...state,
        outdated: false,
        orderPrice: +action.price,
      };
    default:
      return state;
  }
};

export default order;
