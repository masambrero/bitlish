import { combineReducers } from 'redux';
import order from './order';
import modal from './modal';

const rootReducer = combineReducers({
  order,
  modal,
});

export default rootReducer;
