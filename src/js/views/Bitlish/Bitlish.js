import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import CSSTransitionGroup from 'react-transition-group/CSSTransitionGroup';
import {
  receiveOrderBook, calculatePrice,
  showModal,
} from 'Actions';
import OrderTable from 'Components/OrderTable/OrderTable';
import OrderForm from 'Components/OrderForm/OrderForm';
import RequestInfo from 'Components/RequestInfo/RequestInfo';
import './Bitlish.css';

class Bitlish extends Component {
  static renderFirstChild(component) {
    const childrenArray = React.Children.toArray(component.children);
    return childrenArray[0] || null;
  }

  constructor(props) {
    super(props);
    this.socket = new WebSocket('wss://bitlish.com/ws');
    this.socket.onopen = () => {
      this.requestOrderBook();
      setInterval(this.requestOrderBook, 30000);
    };
    this.socket.onmessage = (event) => {
      const response = JSON.parse(event.data);
      let orderBook;
      if (response.call === 'trades_depth') {
        orderBook = { ask: response.data.ask, bid: response.data.bid };
        this.props.receiveOrderBook(orderBook);
        this.setState({ orderTableShown: true });
      }
    };
    this.socket.onerror = error => (this.props.showModal(error));
    this.socket.onclose = () => (this.props.showModal('WebSocket закрылся, сорян.', true));
    this.state = { orderTableShown: false };
  }

  componentWillUnmount() {
    this.socket.close();
  }

  requestOrderBook = () => {
    this.socket.send(JSON.stringify({ data: { pair_id: 'btcusd' }, call: 'trades_depth' }));
  }

  re

  render() {
    return (
      <div className="bitlish">
        <CSSTransitionGroup
          component={this.renderFirstChild}
          transitionName="order-book"
          transitionEnterTimeout={1000}
          transitionLeave={false}
        >
          {this.state.orderTableShown &&
            <div className="bitlish__board">
              <OrderTable orderBook={this.props.orderBook} />
              <OrderForm
                calculatePrice={this.props.calculatePrice}
                outdated={this.props.outdated}
              />
              <RequestInfo
                orderPrice={this.props.orderPrice}
              />
            </div>
          }
        </CSSTransitionGroup>
      </div>
    );
  }
}

Bitlish.defaultProps = {
  orderBook: null,
};

Bitlish.propTypes = {
  receiveOrderBook: PropTypes.func.isRequired,
  calculatePrice: PropTypes.func.isRequired,
  showModal: PropTypes.func.isRequired,
  orderBook: PropTypes.shape({
    ask: PropTypes.arrayOf(PropTypes.shape({
      price: PropTypes.string.isRequired,
      volume: PropTypes.string.isRequired,
    })),
    bid: PropTypes.arrayOf(PropTypes.shape({
      price: PropTypes.string.isRequired,
      volume: PropTypes.string.isRequired,
    })),
  }),
  orderPrice: PropTypes.number.isRequired,
  outdated: PropTypes.bool.isRequired,
};

const mapStateToProps = state => ({
  orderBook: state.order.orderBook,
  orderPrice: state.order.orderPrice,
  outdated: state.order.outdated,
});

const mapDispatchToProps = dispatch => ({
  receiveOrderBook: bindActionCreators(receiveOrderBook, dispatch),
  showModal: bindActionCreators(showModal, dispatch),
  calculatePrice: bindActionCreators(calculatePrice, dispatch),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(Bitlish);
