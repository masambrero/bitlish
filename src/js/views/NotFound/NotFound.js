import React from 'react';

const NotFound = () => (
  <div className="content__page">
    <div className="notFoundTab content__padding">
      <p>Page not found. We are sorry.</p>
      <div className="notFoundTab__img" />
    </div>
  </div>
);

export default NotFound;
