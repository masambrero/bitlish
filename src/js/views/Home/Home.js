import React from 'react';
import { Link } from 'react-router';
import './Home.css';

const Home = () => (
  <div className="home">
    <div className="home__logo">
      <div className="logo">
        <h1 className="logo__header">Трэйданём немножечко?</h1>
        <Link
          to="/bitlish"
          className="logo__link"
        >
          О да!
        </Link>
      </div>
    </div>
  </div>
);

export default (Home);
