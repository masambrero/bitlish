import React, { Component } from 'react';
import { PropTypes } from 'prop-types';
import './OrderForm.css';

class OrderForm extends Component {
  constructor(props) {
    super(props);
    this.state = { orderType: 'ask', orderValue: '' };
  }

  componentWillReceiveProps(nextProps) {
    return (nextProps.outdated === true && nextProps.outdated !== this.props.outdated) ? this.props.calculatePrice(this.state) : false;
  }

  onChangeOrderValue = (event) => {
    const newOrder = { ...this.state, orderValue: +event.target.value };
    if (event.target.value <= 0) return false;
    this.props.calculatePrice(newOrder);
    return this.setState(newOrder);
  }

  onChangeOrderType = (event) => {
    const newOrder = { ...this.state, orderType: event.target.value };
    this.props.calculatePrice(newOrder);
    return this.setState(newOrder);
  }

  render() {
    return (
      <div className="request-field" >
        <h3 className="request-field__header">
          Покупка/продажа:
        </h3>
        <form className="request-field__form order-form">
          <input type="number" value={this.state.orderValue} onChange={this.onChangeOrderValue} />
          <select value={this.state.orderType} onChange={this.onChangeOrderType}>
            <option value="ask">Продать</option>
            <option value="bid">Купить</option>
          </select>
        </form>
      </div>
    );
  }
}

OrderForm.propTypes = {
  calculatePrice: PropTypes.func.isRequired,
  outdated: PropTypes.bool.isRequired,
};

export default OrderForm;
