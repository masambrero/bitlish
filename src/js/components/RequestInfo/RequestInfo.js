import React from 'react';
import { PropTypes } from 'prop-types';
import './RequestInfo.css';

const RequestInfo = ({ orderPrice }) => (
  <div className="request-info">
    <h3 className="request-info__header">
      Итоговая сумма:
    </h3>
    <div className="request-info__total">
      {orderPrice}
    </div>
  </div>
);

RequestInfo.propTypes = {
  orderPrice: PropTypes.number.isRequired,
};

export default RequestInfo;
