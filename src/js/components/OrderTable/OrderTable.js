import React from 'react';
import PropTypes from 'prop-types';
import './OrderTable.css';

// Изменить таблицы с values на keys
const OrderTable = ({ orderBook }) => (
  (
    <div className="order-book">
      {Object.keys(orderBook).map(orderType => (
        <table
          key={orderType}
          className="order-book__table"
        >
          <caption>
            {orderType === 'ask' ? 'ASK (Ордер на продажу)' : 'BID (Ордер на покупку)'}
          </caption>
          <thead>
            <tr>
              <td>
                Volume:
              </td>
              <td>
                Price:
              </td>
            </tr>
          </thead>
          <tbody>
            {orderBook[orderType].map((orderValues, valueIndex) => (
              <tr key={valueIndex}>
                <td>
                  {orderValues.volume}
                </td>
                <td>
                  {orderValues.price}
                </td>
              </tr>
            ))}
          </tbody>
        </table>
      ))}
    </div>
  )
);

OrderTable.propTypes = {
  orderBook: PropTypes.shape({
    ask: PropTypes.arrayOf(PropTypes.shape({
      price: PropTypes.string.isRequired,
      volume: PropTypes.string.isRequired,
    })),
    bid: PropTypes.arrayOf(PropTypes.shape({
      price: PropTypes.string.isRequired,
      volume: PropTypes.string.isRequired,
    })),
  }).isRequired,
};

export default OrderTable;
