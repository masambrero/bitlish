const path = require("path");
const webpack = require("webpack");
const ExtractTextPlugin = require("extract-text-webpack-plugin");
const HtmlWebpackPlugin = require("html-webpack-plugin");

module.exports = {
  context: path.resolve(__dirname, 'src'),
  entry: {
    app: ['./js/index.js'],
    vendor: ['react', 'react-dom', 'react-router']
  },
  output: {
    path: path.resolve(__dirname, 'public'),
    publicPath: '/',
    filename: '[name].js'
  },
  module: {
    rules: [
      {
        test: /\.jsx?$/,
        enforce: 'pre',
        exclude: [
          /node_modules/
        ],
        use: [{
          loader: 'eslint-loader',
          options: {
            configure: '.eslintrc',
            failOnWarning: false,
            failOnError: false
          }
        }]
      },
      {
        test: /\.js/,
        exclude: [
          /node_modules/
        ],
        use: 'babel-loader'
      },
      {
        test: /\.css$/,
        use: ExtractTextPlugin.extract({
          fallback: "style-loader",
          use: [
            'css-loader?importLoaders=1',
            'postcss-loader'
          ]
        })
      },
      {
        test: /\.(png|jpe?g|gif|svg)$/,
        use: 'url-loader?name=[path][name].[ext]&limit=4096'
      }
    ]
  },
  resolve: {
    alias: {
      Actions: path.resolve(__dirname, 'src/js/actions/'),
      Components: path.resolve(__dirname, 'src/js/components/'),
      Constants: path.resolve(__dirname, 'src/js/constants/'),
      Containers: path.resolve(__dirname, 'src/js/containers/'),
      Views: path.resolve(__dirname, 'src/js/views/'),
    }
  },
  devServer: {
    publicPath: '/',
    hot: true,
    contentBase: path.resolve(__dirname, 'public'),
    historyApiFallback: true,
  },
  plugins: [
    new webpack.HotModuleReplacementPlugin(),
    new ExtractTextPlugin({
      filename: './css/style.css',
      allChunks: true
    }),
    new webpack.optimize.CommonsChunkPlugin({
      name: 'vendor',
      minChunks: Infinity
    }),
    new HtmlWebpackPlugin({
      template: './index.html',
      filename: 'index.html',
      inject: 'body'
    }),
  ]
};
